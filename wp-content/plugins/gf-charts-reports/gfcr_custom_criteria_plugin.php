<?php


if (! class_exists('gfcr_custom_criteria_plugin')) {
    class gfcr_custom_criteria_plugin
    {
        function __construct()
        {
            //Logger::getLogger("default")->debug("construct gfcr_custom_search_criteria");
            Logger::getLogger("default")->debug ("Adding Module : ".__CLASS__);
            add_filter("gfcr_modify_custom_search_criteria", array(
                $this,
                "modify_custom_search_criteria"
            ));
        }
        
        
        function modify_custom_search_criteria($search_criteria)
        {
            Logger::getLogger("default")->debug("modify_custom_search_criteria BEFORE");
            Logger::getLogger("default")->debug($search_criteria);
            foreach ($search_criteria as $key => $value) {
                //Logger::getLogger("default")->debug($value);
                if ($key == 'field_filters') {
                    //Logger::getLogger("default")->debug($key);
                    foreach ($value as $key2 => $value2) {
                        //Logger::getLogger("default")->debug($value2);
                        if (is_array($value2) && $value2['key'] == 'created_by') {
                            //Logger::getLogger("default")->debug($value2['key'].' found with val: '.$value2['value']);
                            if ($value2['value'] == 'current') {
                                // replace by current user id
                                $newVal = get_current_user_id();
                                Logger::getLogger("default")->debug('need to replace '.$value2['value'].' with val: '.$newVal);
                                $search_criteria[$key][$key2]['value'] = $newVal;
                            }
            
                        }
                    }
                } else if ($key === 'date_range') {
                    // interpret string like date range, and set correct value to GF criteria filter
                    if ($value === "last_week" ) {
                        $start_date = date( 'Y-m-d', strtotime('-7 days') );
                        $end_date = date( 'Y-m-d', time() );
                    } else if ($value === "last_month" ) {
                        $start_date = date( 'Y-m-d', strtotime('-30 days') );
                        $end_date = date( 'Y-m-d', time() );
                    } else if ($value === "last_year" ) {
                        $start_date = date( 'Y-m-d', strtotime('-12 month') );
                        $end_date = date( 'Y-m-d', time() );
                    }
                    
                    //"start_date":"2016-10-12","end_date":"2017-12-01"
                    $search_criteria["start_date"] = $start_date;
                    $search_criteria["end_date"] = $end_date;
                }
            }

            Logger::getLogger("default")->debug("modify_custom_search_criteria AFTER");
            Logger::getLogger("default")->debug($search_criteria);
            return $search_criteria;
        }
    }
}