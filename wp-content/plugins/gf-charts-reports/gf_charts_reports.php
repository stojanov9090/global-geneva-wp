<?php
/*
 * Plugin Name: Gravity Forms Charts Reports
* Plugin URI: http://wordpress.org/plugins/gravity-charts/
* Description: Create amazing HTML5 charts from Gravity Forms submission data with a simple shortcode. Links to chart.js lib : http://www.chartjs.org/.
* Version: 2.5.4
* Author: Termel
* Author URI: http://profiles.wordpress.org/munger41/
*/
define ( 'GFCHARTSREPORTS_URL', trailingslashit ( plugins_url ( '', __FILE__ ) ) );
define ( 'GFCHARTSREPORTS_PATH', trailingslashit ( plugin_dir_path ( __FILE__ ) ) );
define ( 'GFCHARTSREPORTS_BASENAME', plugin_basename ( __FILE__ ) );

require_once __DIR__ . '/libs/vendor/autoload.php';
use League\Csv\Reader;
use League\Csv\Statement;

if (! function_exists ( 'gfchartreports_log' )) {
	function gfchartreports_log($message) {
		if (WP_DEBUG === true) {
			if (is_array ( $message ) || is_object ( $message )) {
				error_log ( print_r ( $message, true ) );
			} else {
				error_log ( $message );
			}
		}
	}
}

if (! class_exists ( 'gfchartsreports' )) {
	class gfchartsreports {
		protected $criteria_module = NULL;
		protected $table_module = NULL;
		function setUpLogger() {
			gfchartreports_log ( "#################################" );
			$log4phpVersion = '2.3.0';
			$path_to_log4php = sprintf ( '%s/libs/log4php/' . $log4phpVersion . '/Logger.php', dirname ( __FILE__ ) );
			gfchartreports_log ( $path_to_log4php );
			if (! class_exists ( 'LoggerConfiguratorDefault' )) {
				include_once ($path_to_log4php);
			}
				
			$configurator = new LoggerConfiguratorDefault ();
			$fileConfig = false;
			if (! $fileConfig) {
				gfchartreports_log ( "PHP config log" );
				$dir = plugin_dir_path ( __FILE__ );
				gfchartreports_log ( $dir );
				$logFilesPath = $dir . '/logs/gfcr-%s.log';
				gfchartreports_log ( $logFilesPath );

				Logger::configure ( array (
						'rootLogger' => array (
								'appenders' => array (
										'default'
								),
								'level' => 'debug'
						),
						'appenders' => array (
								'default' => array (
										'class' => 'LoggerAppenderDailyFile',
										'layout' => array (
												'class' => 'LoggerLayoutPattern',
												'params' => array (
														'conversionPattern' => "%date{Y-m-d H:i:s,u} %logger %-5level %F{10}:%L %msg%n"
												)
										)
										,
										/*'file' => strval($logFilesPath),*/
										'params' => array (
												'file' => strval ( $logFilesPath ),
												'append' => true,
												'datePattern' => "Y-m-d"
										)
								)
						)
				) );
			} else {

				$dir = plugin_dir_path ( __FILE__ );
				$configPath = realpath ( $dir . '/logs/config.xml' );
				gfchartreports_log ( $configPath );
				$config = $configurator->parse ( $configPath );
				Logger::configure ( $config );
			}
		}
		function __construct() {
			add_action ( "wp_enqueue_scripts", array (
					$this,
					"gfchartsreports_load_scripts"
			) );
			add_action ( 'wp_head', array (
					$this,
					'gfchartsreports_html5_support'
			) );
			add_shortcode ( 'gfchartsreports', array (
					$this,
					'gfchartsreports_shortcode'
			) );
			add_shortcode ( 'csv2chartjs', array (
					$this,
					'csv2chartjs_shortcode'
			) );
			add_shortcode ( 'gfd3reports', array (
					$this,
					'gfd3reports_shortcode'
			) );
				
			$this->setUpLogger ();
			gfchartreports_log ( "Done" );
			Logger::getLogger ( "default" )->debug ( "Logger initialized successfully" );
				
			$modulesArray = array (
					'gfcr_custom_criteria_plugin',
					'gfcr_data_conversion_plugin',
					'gfcr_table_plugin'
			);
				
			foreach ( $modulesArray as $moduleFile ) {
				if (! class_exists ( $moduleFile )) {
					$criteria_module_path = plugin_dir_path ( __FILE__ ) . $moduleFile . '.php';
					if (file_exists ( $criteria_module_path )) {
						// Logger::getLogger("default")->info("include file : " . $criteria_module_path);
						include_once $criteria_module_path;
					} else {
						Logger::getLogger ( "default" )->warn ( "No module file : " . $criteria_module_path );
						continue;
					}
				}
				// $className = "Class".$str;
				if (class_exists ( $moduleFile )) {
					$object = new $moduleFile ();
					// Logger::getLogger("default")->debug("Module " . $moduleFile . " Loaded");
				} else {
					Logger::getLogger ( "default" )->warn ( "File found : " . $criteria_module_path . ' but no class ' . $moduleFile );
				}
			}
			/*
			 * $criteria_module_path = plugin_dir_path(__FILE__) . 'gfcr_custom_criteria_plugin.php';
			 * Logger::getLogger("default")->debug ($criteria_module_path);
			 * include_once $criteria_module_path;
			 *
			 * if (class_exists('gfcr_custom_criteria_plugin')) {
			 * Logger::getLogger("default")->debug ("Class exists gfcr_custom_search_criteria");
			 * $this->criteria_module = new gfcr_custom_search_criteria();
			 *
			 * } else {
			 * Logger::getLogger("default")->debug ("Class DOES not exist gfcr_custom_search_criteria");
			 * }
			 */
		}
		function csv_to_array($filename) {
			Logger::getLogger ( "default" )->debug ( "###::csv_to_array:" . $filename );
			$errorMsg = '';
			$realPathFilename = realpath ( $filename );
			$results = array ();
				
			if (file_exists ( $realPathFilename )) {
				Logger::getLogger ( "default" )->debug ( "server path :" . $realPathFilename );
				if (is_readable ( $realPathFilename )) {
					try {
						$reader = Reader::createFromPath ( $realPathFilename );
						$results = $reader->fetchAll ();
					} catch ( Exception $e ) {
						// Handle exception
						$errorMsg = 'cannot read file : ' . $realPathFilename;
						// Logger::getLogger("default")->error("file not readable :" . $realPathFilename);
					}
				} else {
					$errorMsg = 'cannot read file : ' . $realPathFilename;
					// Logger::getLogger("default")->error("file not readable :" . $realPathFilename);
				}
			} else if (filter_var ( $filename, FILTER_VALIDATE_URL ) !== FALSE) {
				Logger::getLogger ( "default" )->debug ( "url used :" . $filename );
				try {
					$content = file_get_contents ( $filename );
					Logger::getLogger ( "default" )->debug ( $content );
					if ($content === false) {
						// Handle the error
						$errorMsg = 'file is empty : ' . $filename;
					} else {
						// $rows = explode("\n",$content);
						$rows = explode ( PHP_EOL, $content );
						// Logger::getLogger("default")->debug($rows);
						// $results = array();
						foreach ( $rows as $row ) {
							$results [] = str_getcsv ( $row );
						}
						// Logger::getLogger("default")->debug($results);
					}
				} catch ( Exception $e ) {
					// Handle exception
					$errorMsg = 'cannot read file at url : ' . $filename;
				}
			}
				
			if (! empty ( $errorMsg )) {
				Logger::getLogger ( "default" )->error ( $errorMsg );
				return false;
			}
				
			return $results;
		}

		/**
		 * Add IE Fallback for HTML5 and canvas
		 *
		 * @since Unknown
		 */
		function gfchartsreports_html5_support() {
			echo '<!--[if lte IE 8]>';
			echo '<script src="' . plugins_url ( '/js/excanvas.js', __FILE__ ) . '"></script>';
			echo '<![endif]-->';
			echo '	<style>
    			/*gfchartsreports_js responsive canvas CSS override*/
    			.gfchartsreports_canvas {
    				width:100%!important;
    				max-width:100%;
    			}

    			@media screen and (max-width:480px) {
    				div.gfchartsreports-wrap {
    					width:100%!important;
    					float: none!important;
						margin-left: auto!important;
						margin-right: auto!important;
						text-align: center;
    				}
    			}
    		</style>';
		}

		/**
		 * Register Script
		 *
		 * @since Unknown
		 */
		function gfchartsreports_load_scripts($force = false) {
			if (! is_admin () || $force) {
				wp_enqueue_script ( 'jquery' );
				$chartJsVersion = "2.5.0";
				// $chartJsVersion = "2.1.6";
				$chartJsCDN = plugins_url ( "libs/chartsjs/" . $chartJsVersion . "/Chart.min.js", __FILE__ );

				wp_register_script ( 'chart-js', $chartJsCDN, null, null );

				wp_register_script ( 'gfchartsreports-functions', GFCHARTSREPORTS_URL . '/js/functions.js', array (
						'jquery'
				), '', true );

				wp_enqueue_script ( 'chart-js' );
				wp_enqueue_script ( 'gfchartsreports-functions' );
			}
		}
		function gfd3reports_shortcode($atts) {
			$source = 'gf';
			$destination = 'd3';
			return $this->chartReports ( $source, $destination, $atts );
		}
		// $a = array(1, 2, 3, 4, 5);
		function gfchartsreports_shortcode($atts) {
			$source = 'gf';
			$destination = 'chartjs';
			return $this->chartReports ( $source, $destination, $atts );
		}
		function csv2chartjs_shortcode($atts) {
			$source = 'csv';
			$destination = 'chartjs';
			return $this->chartReports ( $source, $destination, $atts );
		}
		function setColorPalette($color_set, $color_rand, &$colors) {
			Logger::getLogger ( "default" )->debug ( "SETTING COLOR PALETTE..." );
			if (! empty ( $color_set )) {
				Logger::getLogger ( "default" )->debug ( "### Color Set is : " . $color_set );
				switch ($color_set) {
					case 'blue' :
						$colors = array (
						'#ffffd9',
						'#edf8b1',
						'#c7e9b4',
						'#7fcdbb',
						'#41b6c4',
						'#1d91c0',
						'#225ea8',
						'#253494',
						'#081d58'
								);
						break;
					case 'red' :
						$colors = array (
						'#ffffcc',
						'#ffeda0',
						'#fed976',
						'#feb24c',
						'#fd8d3c',
						'#fc4e2a',
						'#e31a1c',
						'#bd0026'
								);
						break;
					case 'orange' :
						$colors = array (
						'#ffffe5',
						'#fff7bc',
						'#fee391',
						'#fec44f',
						'#fe9929',
						'#ec7014',
						'#cc4c02',
						'#993404',
						'#662506'
								);

						break;
					case 'green' :
						$colors = array (
						'#ffffe5',
						'#f7fcb9',
						'#d9f0a3',
						'#addd8e',
						'#78c679',
						'#41ab5d',
						'#238443',
						'#006837',
						'#004529'
								);
						break;
							
					case 'purple' :
						$colors = array (
						'#fff7f3',
						'#fde0dd',
						'#fcc5c0',
						'#fa9fb5',
						'#f768a1',
						'#dd3497',
						'#ae017e',
						'#7a0177',
						'#49006a'
								);
						break;
				}
			} else if (! empty ( $colors )) {
				$colors = explode ( ',', $colors );
				Logger::getLogger ( "default" )->debug ( "### User colors : " . count ( $colors ) );
			} else {

				$colors = array (
						'#a6cee3',
						'#1f78b4',
						'#b2df8a',
						'#33a02c',
						'#fb9a99',
						'#e31a1c',
						'#fdbf6f',
						'#ff7f00',
						'#cab2d6',
						'#6a3d9a',
						'#ffff99',
						'#b15928'
				);
				Logger::getLogger ( "default" )->debug ( "### Default colors : " . count ( $colors ) );
			}
				
			if (! empty ( $color_rand ) && $color_rand == true) {
				Logger::getLogger ( "default" )->debug ( "### Shuffled colors : " . count ( $colors ) );
				shuffle ( $colors );
			}
		}
		function csvColumnAnalysis($csvArray, $header_start, $header_size, $xCol, $yCol, $type, $compute, $maxentries) {
			$collectionArray = array ();
			Logger::getLogger ( "default" )->debug ( "### Options : " . implode ( '/', array (
					$header_start,
					$header_size,
					$xCol,
					$yCol,
					$type,
					$compute,
					$maxentries
			) ) );
			$idx = 0;
			$title = "";
			$results = array ();
			if ($compute == 'SUM') {
				foreach ( $csvArray as $key => $values ) {
					if ($header_start > $idx) {
						$idx ++;
						continue;
					}
						
					if ($idx >= $maxentries) {
						break;
					}
					if ($header_size >= $idx && $idx > $header_start) {
						$title .= $values [$yCol];
					} else {
						$collectionArray [] = $values [$yCol];
					}
						
					$idx ++;
				}
				$results ['scores'] = array_count_values ( $collectionArray );
				$results ['data'] = array_values ( $results ['scores'] );
				$results ['labels'] = array_keys ( $results ['scores'] );
			} else {
				Logger::getLogger ( "default" )->debug ( "#### Build chart with max " . $maxentries . " CSV datas of column " . $yCol );
				foreach ( $csvArray as $key => $values ) {
						
					if ($header_start > $idx) {
						Logger::getLogger ( "default" )->warn ( 'skip ' . $idx );
						$idx ++;

						continue;
					}
					if ($idx >= $maxentries) {
						Logger::getLogger ( "default" )->warn ( 'max reached ' . $maxentries );
						break;
					}
					if (($header_start + $header_size) > $idx && $idx > $header_start) {
						$title .= empty ( $values [$yCol] ) ? '' : $values [$yCol] . '\n';
						Logger::getLogger ( "default" )->warn ( '---------- title: ' . $title );
						$idx ++;
						continue;
					} else {

						$valueToCatch = $values [$yCol];
						$trimed = $this->clean ( $valueToCatch );
						Logger::getLogger ( "default" )->trace ( $valueToCatch . ' => ' . $trimed );
						if (is_numeric ( $trimed )) {
							$valueToCatch = $trimed; // str_replace (' ','',$values [$yCol]);
						} else {
							/*
							 * $idx ++;
							 * continue;
							 */
							$valueToCatch = '"' . $this->removeQuotesAndConvertHtml ( $valueToCatch ) . '"';
						}

						$collectionArray [$values [$xCol]] = $valueToCatch;
						Logger::getLogger ( "default" )->debug ( $idx . ' :: ' . $values [$xCol] . ' -> ' . $values [$yCol] );
					}
					$idx ++;
				}

				Logger::getLogger ( "default" )->debug ( $collectionArray );
				$results ['data'] = array_values ( $collectionArray );
				$results ['labels'] = array_keys ( $collectionArray );
			}
				
			if (! empty ( $title )) {
				$results ['label'] = $title;
			}
				
			return $results;
		}
		function countAnswers($reportFields, $entries) {
			$countArray = array ();
			foreach ( $reportFields as $fieldId => $fieldData ) {
				if (empty ( $fieldId )) {
					Logger::getLogger ( "default" )->debug ( 'empty field ' . $fieldId );
					continue;
				}

				$fieldType = $fieldData ['type'];
				$multiRows = isset ( $fieldData ['gsurveyLikertEnableMultipleRows'] ) ? $fieldData ['gsurveyLikertEnableMultipleRows'] == 1 : false;
				$multiText = ($multiRows ? 'multirows' : 'single row');
				Logger::getLogger ( "default" )->debug ( "--> Counting answers in entries for field " . $fieldType . ' (' . $multiText . ') : ' . $fieldId );

				// Logger::getLogger("default")->debug ( $fieldData );
				$countArray [$fieldId] = array ();
				foreach ( $entries as $entry ) {
					// Logger::getLogger("default")->debug ("--> entry ".$entry['id']);
					// Logger::getLogger("default")->debug ($entry);
					foreach ( $entry as $key => $value ) {
						if (empty ( $key ) || empty ( $value )) {
							continue;
						} else {
							Logger::getLogger ( "default" )->trace ( $key . " => " . $value );
						}

						if ($fieldType == 'checkbox') {
							/*
							 * $pattern = '/^' . $fieldId . './';
							 * preg_match($pattern, $key, $matches);
							 */
							$keyExploded = explode ( '.', $key );
							if (isset ( $keyExploded [0] ) && isset ( $keyExploded [1] ) && $keyExploded [0] == $fieldId) {
								// Logger::getLogger("default")->debug('Pattern found : ' . $key . " <--> " . $fieldId);
								// Logger::getLogger("default")->debug( $keyExploded[0] . " / " . $keyExploded[1]);
								// $labelForChoice = $reportFields [$fieldId] ['choices'][$keyExploded[1]-1]['label'];
								$labelForChoice = $reportFields [$fieldId] ['choices'] [$keyExploded [1] - 1] ['text'];
								$countArray [$fieldId] ['answers'] [] = $labelForChoice;
							}
						} else {
							// Logger::getLogger("default")->debug ($key." <- MATCHES ??? -> ".$fieldId);
							if (trim ( $key ) == trim ( $fieldId ) || ($multiRows && strpos ( trim ( $key ), trim ( $fieldId . '.' ) ) !== false)) {
								Logger::getLogger ( "default" )->trace ( $key . " <- MATCHES -> " . $fieldId );
								if ($fieldType == 'survey') {
										
									if ($multiRows) {
										//Logger::getLogger ( "default" )->debug ( "### MULTI ROW SURVEY FIELD ###" );
										// Logger::getLogger("default")->debug ($value);
										// need to get label instead of value!
										$newValue = $value;
									} else {
										Logger::getLogger ( "default" )->debug ( "### SINGLE ROW SURVEY FIELD ###" );
										// need to get label instead of value!
										if (! is_array ( $fieldData )) {
											Logger::getLogger ( "default" )->warn ( "not an array : " . $fieldData );
											continue;
										}

										foreach ( $fieldData as $k => $v ) {
											if (! is_array ( $v )) {
												Logger::getLogger ( "default" )->warn ( "not an array : " . $v );
												continue;
											}
											foreach ( $v as $keyIdx => $originalChoice ) {
												// Logger::getLogger("default")->debug ( $originalChoice );
												if (trim ( $originalChoice ['value'] ) == trim ( $value )) {
													$newValue = trim ( $originalChoice ['text'] );
												}
											}
										}
									}
								} else {
									$newValue = $value;
								}
								// Logger::getLogger("default")->debug($key .' ==? '.$fieldId);
								// Logger::getLogger("default")->debug ( "Found new answer for field id " . $key );
								// Logger::getLogger("default")->debug ( $value );
								$countArray [$fieldId] ['answers'] [] = $newValue;
							}
						}
					}
				}
				// Logger::getLogger("default")->debug($nbOfEntries . " entries parsed for field " . $fieldId);
			}
			return $countArray;
		}
		function computeScores($countArray, $reportFields) {
			if (empty ( $countArray )) {
				$msg = "Error make count of answers";
				Logger::getLogger ( "default" )->error ( $msg );
				return $msg;
			}
				
			foreach ( $countArray as $fieldId => $fieldValues ) {
				if (! isset ( $fieldValues ['answers'] )) {
					Logger::getLogger ( "default" )->warn ( "No answers for field " . $fieldId );
					$reportFields [$fieldId] ['no_answers'] = 1;
					continue;
				}
				$answers = $fieldValues ['answers'];
				Logger::getLogger ( "default" )->debug ( "--> Computing score for field " . $fieldId );
				// Logger::getLogger("default")->debug ($answers);
				$reportFields [$fieldId] ['scores'] = array_count_values ( $answers );
			}
				
			return $reportFields;
		}
		function countDataFor($source, $entries, $reportFields, $data_conversion, $includeArray = null) {
			// $countArray = array();
			Logger::getLogger ( "default" )->info ( 'Building ' . count ( $reportFields ) . " fields upon " . count ( $entries ) . " entries" );
			Logger::getLogger ( "default" )->trace ( $entries [0] );
			$countArray = $this->countAnswers ( $reportFields, $entries );
				
			Logger::getLogger ( "default" )->debug ( count ( $countArray ) . ' graph should be displayed' );
			Logger::getLogger ( "default" )->trace ( $countArray );
				
			$reportFields = $this->computeScores ( $countArray, $reportFields );
				
			$chartTitle = __ ( "Complete report of form " ); // . ' ' . $form_id;
				
			Logger::getLogger ( "default" )->debug ( $chartTitle );
			$reportFields ['title'] = $chartTitle;
				
			// conversions
			if ($data_conversion) {
				$reportFields = apply_filters ( 'gfcr_filter_fields_after_count', $reportFields, $data_conversion );
			}
				
			$toDisplay = count ( $reportFields ) - 1;
			Logger::getLogger ( "default" )->debug ( $toDisplay . ' graph should be displayed' );
			Logger::getLogger ( "default" )->trace ( $reportFields );
				
			foreach ( $reportFields as $id => $values ) {
				$scores = isset ( $values ['scores'] ) ? $values ['scores'] : '';
				if (empty ( $scores )) {
					continue;
				}
				// Logger::getLogger("default")->debug ( "scores:" );
				// Logger::getLogger("default")->debug ( $scores );

				$multiRows = isset ( $values ['gsurveyLikertEnableMultipleRows'] ) ? $values ['gsurveyLikertEnableMultipleRows'] == 1 : false;
				if ($multiRows) {
					
					foreach ( $scores as $scoreKey => $scoreValue ) {
						$xyValues = explode ( ':', $scoreKey );
						$datasetName = $xyValues [0];
						$datasetVal = $xyValues [1];
						// Logger::getLogger("default")->debug ( $values['inputs'] );
						foreach ( $values ['inputs'] as $inputIdx => $inputData ) {
							if (trim ( $inputData ['name'] ) == $datasetName) {
								$datasetNameLabel = trim ( $inputData ['label'] );
							}
						}
						foreach ( $values ['choices'] as $inputIdx => $inputData ) {
							if (trim ( $inputData ['value'] ) == $datasetVal) {
								$datasetValLabel = trim ( $inputData ['text'] );
							}
						}
					
						if ($values['datasets_invert']) {
							$reportFields [$id] ['datasets'] [$datasetValLabel] ['data'] [$datasetNameLabel] = $scoreValue;
							$reportFields [$id] ['labels'] [] = $datasetNameLabel;
						} else {
							$reportFields [$id] ['datasets'] [$datasetNameLabel] ['data'] [$datasetValLabel] = $scoreValue;
							$reportFields [$id] ['labels'] [] = $datasetValLabel;
						}
					}
				} else {
					$percents = isset ( $values ['percents'] ) ? $values ['percents'] : '';
					$reportFields [$id] ['labels'] = array_keys ( $scores );
					$reportFields [$id] ['data'] = array_values ( $scores );
				}
				// Logger::getLogger("default")->debug ( "labels:" );
				// Logger::getLogger("default")->debug ( $reportFields [$id] ['labels'] );
			}
				
			return $reportFields;
		}
		function getGFEntries($form_id, $maxentries, $custom_search_criteria) {
			$form = GFAPI::get_form ( $form_id );
			$allEntriesNb = GFAPI::count_entries ( $form_id );
			Logger::getLogger ( "default" )->debug ( "All entries (also deleted!) : " . $allEntriesNb );
			$paging = array (
					'offset' => 0,
					'page_size' => $maxentries
			);
			if (empty ( $custom_search_criteria )) {
				$search_criteria ['status'] = 'active';
			} else {
				// '{"a":1,"b":2,"c":3,"d":4,"e":5}'
				// 'field_filters'][] = array( 'key' => 'created_by', 'value' => $current_user->ID );
				// {"status":"active","field_filters":{"0":{"key":"created_by","value":"55"},"1":{"key":"read","value":true}}}
				$search_criteria = json_decode ( $custom_search_criteria, true );

				$search_criteria = apply_filters ( 'gfcr_modify_custom_search_criteria', $search_criteria );
			}
			Logger::getLogger ( "default" )->debug ( "Search crit : " );
			Logger::getLogger ( "default" )->debug ( $search_criteria );
				
			$sorting = null;
			$entries = GFAPI::get_entries ( $form_id, $search_criteria, $sorting, $paging );
			// $entries = GFAPI::get_entries ( $form_id );
			$nbOfEntries = count ( $entries );
				
			Logger::getLogger ( "default" )->debug ( "Create complete report for form " . $form_id );
			Logger::getLogger ( "default" )->debug ( "entries : " . $nbOfEntries );
			return $entries;
		}
		function buildReportFieldsForGF($form_id, $type, $includeArray, $excludeArray = null, $datasets_invert = null) {
			Logger::getLogger ( "default" )->debug ( "GF data to dig " . $form_id );
				
			$form = GFAPI::get_form ( $form_id );
				
			// $graphType = $type;
			$allFields = $form ['fields'];
			Logger::getLogger ( "default" )->debug ( "form fields : " . count ( $allFields ) );
			foreach ( $allFields as $formFieldId => $fieldData ) {

				$fieldType = $fieldData ['type'];
				$fieldId = $fieldData ['id'];
				if (! empty ( $includeArray )) {
					if (! in_array ( $fieldId, $includeArray )) {
						continue;
					}
				} else if (! empty ( $excludeArray )) {
					if (in_array ( $fieldId, $excludeArray )) {
						continue;
					}
				}
				// Logger::getLogger("default")->debug($fieldData);
				Logger::getLogger ( "default" )->debug ( $type . " ### Processing field " . $formFieldId . ' of type ' . $fieldType );
				// $skipField = false;
				// $fieldData = apply_filters('gfcr_filter_gf_field_before_type_process', $formFieldId, $fieldData);

				switch ($fieldType) {
					case 'text' :
					case 'textarea' :
						/*
						 * if (empty($type)){
						 * $type = "table";
						 * break;
						 * } else {
						 * break;
						 * }
						 */
						
					case 'checkbox' :
					case 'radio' :
					case 'survey' :

						if ($fieldData ['gsurveyLikertEnableMultipleRows'] == 1) {
							Logger::getLogger ( "default" )->debug ( "MULTI ROW SURVEY LIKERT" );
							Logger::getLogger ( "default" )->trace ( $fieldData );
							$reportFields [$fieldId] ['choices'] = $fieldData ['choices'];
							$reportFields [$fieldId] ['inputs'] = $fieldData ['inputs'];
							$reportFields [$fieldId] ['gsurveyLikertEnableMultipleRows'] = 1;
							$reportFields [$fieldId] ['multisets'] = 1;
						} else {
							$reportFields [$fieldId] ['choices'] = $fieldData ['choices'];
						}

						// }
						break;
					case 'slider' :
						break;
					case 'number' :
						break;
					case 'select' :
						break;
					default :
						Logger::getLogger ( "default" )->warn ( "Unknown field type : " . $fieldType );

						continue;
						break;
				}

				

				$reportFields [$fieldId] ['datasets_invert'] = $datasets_invert;
				$reportFields [$fieldId] ['label'] = $fieldData ['label'];
				$reportFields [$fieldId] ['graphType'] = $type;
				$reportFields [$fieldId] ['type'] = $fieldType;
				
				Logger::getLogger ( "default" )->debug ( "Creating report field  : " . $fieldData ['label'] . ' of type ' . $fieldType . ' -> ' . $type .' inverted:'.$datasets_invert);
			}
				
			return $reportFields;
		}
		function chartReports($source, $destination, $atts) {
			// Default Attributes
			// - - - - - - - - - - - - - - - - - - - - - - -
			extract ( shortcode_atts ( array (
					'type' => 'pie',
					'url' => '',
					'float' => false,
					'center' => false,
					'title' => 'chart',
					'canvaswidth' => '625',
					'canvasheight' => '625',
					'width' => '48%',
					'height' => 'auto',
					'margin' => '5px',
					'relativewidth' => '1',
					'align' => '',
					'class' => '',
					'labels' => '',
					'data' => '30,50,100',
					'data_conversion' => '',
					'datasets_invert' => '',
					'datasets' => '',
					'gf_form_ids' => '',
					'multi_include' => '',
					'gf_form_id' => '1',
					'maxentries' => '200',
					'gf_criteria' => '',
					'include' => '',
					'exclude' => '',
					'colors' => '',
					'color_set' => '',
					'color_rand' => false,
					'chart_js_options' => '',
					'tooltip_style' => 'BOTH',
					'custom_search_criteria' => '',
					'fillopacity' => '0.7',
					'pointstrokecolor' => '#FFFFFF',
					'animation' => 'true',
					'scalefontsize' => '12',
					'scalefontcolor' => '#666',
					'scaleoverride' => 'false',
					'scalesteps' => 'null',
					'scalestepwidth' => 'null',
					'scalestartvalue' => 'null',
						
					'xcol' => '0',
					'ycol' => '1',
					'compute' => '',
					'header_start' => '0',
					'header_size' => '1'
			), $atts ) );
				
			Logger::getLogger ( "default" )->debug ( $atts );
				
			// - - - - - - - - - - - - - - - - - - - - - - -
			$type = str_replace ( ' ', '', $type );
			$url = str_replace ( ' ', '', $url );
			$title = str_replace ( ' ', '', $title );
			$data = explode ( ',', str_replace ( ' ', '', $data ) );
			$data_conversion = str_replace ( ' ', '', $data_conversion );
			$datasets_invert = str_replace ( ' ', '', $datasets_invert );
			$datasets = explode ( "next", str_replace ( ' ', '', $datasets ) );
			$gf_form_ids = explode ( ',', str_replace ( ' ', '', $gf_form_ids ) );
			$multi_include = explode ( ',', str_replace ( ' ', '', $multi_include ) );
			$gf_form_id = str_replace ( ' ', '', $gf_form_id );
				
			$colors = str_replace ( ' ', '', $colors );
			$color_set = str_replace ( ' ', '', $color_set );
			$color_rand = str_replace ( ' ', '', $color_rand );
			$float = str_replace ( ' ', '', $float );
			$center = str_replace ( ' ', '', $center );
			$include = str_replace ( ' ', '', $include );
			$exclude = str_replace ( ' ', '', $exclude );
			$custom_search_criteria = str_replace ( ' ', '', $custom_search_criteria );
			$tooltip_style = str_replace ( ' ', '', $tooltip_style );
			$xcol = str_replace ( ' ', '', $xcol );
			$compute = str_replace ( ' ', '', $compute );
			$maxentries = str_replace ( ' ', '', $maxentries );
			$header_start = str_replace ( ' ', '', $header_start );
			$header_size = str_replace ( ' ', '', $header_size );
				
			$reportFields = array ();
			if ((! empty ( $include ))) {
				$includeArray = explode ( ",", $include );
			}
			if (! empty ( $exclude )) {
				$excludeArray = explode ( ",", $exclude );
			}
			Logger::getLogger ( "default" )->debug ( $type );
				
			if (empty ( $source ) || (empty ( $destination ))) {
				$msg = "Invalid source and/or destination " . $source . ' / ' . $destination;
				Logger::getLogger ( "default" )->error ( $msg );
				return $msg;
			}
				
			if ($source == 'gf'/* && ! empty($gf_form_id) && $gf_form_id > 0*/) {
				if (! function_exists ( 'is_plugin_active' )) {
					// add plugin active check function
					include_once (ABSPATH . 'wp-admin/includes/plugin.php');
				}
				if (! class_exists ( 'GFCommon' ) && ! is_plugin_active ( 'gravityforms/gravityforms.php' )) {
					// check if gravity forms installed and active
					$msg = "Please install/activate gravityforms plugin";
					Logger::getLogger ( "default" )->error ( $msg );
					return $msg;
				}

				if (! empty ( $gf_form_ids ) && count ( $gf_form_ids ) > 1 && ! empty ( $multi_include ) && count ( $multi_include ) > 1) {
					// process multi-form sources
					Logger::getLogger ( "default" )->info ( "#### MULTI sources process" );
						
					$multiCombined = array_combine ( $gf_form_ids, $multi_include );
					Logger::getLogger ( "default" )->info ( $multiCombined );
					$countArray = array ();
					foreach ( $multiCombined as $gf_id => $field_id ) {
						Logger::getLogger ( "default" )->info ( "#### MULTI " . $gf_id . ' -> ' . $field_id );
						$entries = $this->getGFEntries ( $gf_id, $maxentries, $custom_search_criteria );
						$currentReportFields = $this->buildReportFieldsForGF ( $gf_id, $type, array (
								$field_id
						), null, $datasets_invert );
						// $reportFieldsArray = $this->countDataFor($source, $entries, $reportFieldsArray, $data_conversion);
						// Logger::getLogger("default")->debug($reportFieldsArray);
						Logger::getLogger ( "default" )->info ( "#### MULTI Counting " . $gf_id . ' -> ' . $field_id );
						$currentCount = $this->countAnswers ( $currentReportFields, $entries );
						$reportFieldsArray [] = $currentReportFields;
						/*
						 * Logger::getLogger("default")->debug($currentCount);
						 */
						$answers = reset ( $currentCount ) ['answers'];
						// Logger::getLogger("default")->debug($answers);
						$mergedAnswers = array_merge ( $mergedAnswers, $answers );
						$countArray [] = $currentCount;
						// $rpa[] = $reportFieldsArray;
						// $reportFields = $reportFieldsArray;
					}
						
					Logger::getLogger ( "default" )->debug ( "#### MULTI DATA RETRIEVED " . count ( $reportFieldsArray ) . ' graph should be merged' );
					Logger::getLogger ( "default" )->debug ( $countArray );
					Logger::getLogger ( "default" )->debug ( $reportFieldsArray );
					$reportFields = reset ( $reportFieldsArray );
					Logger::getLogger ( "default" )->debug ( $reportFields );
						
					Logger::getLogger ( "default" )->debug ( array_search ( "answers", $countArray ) );
					/*
					 * foreach ($countArray as $key => $value) {
					 * $reportFields[$key]['answers'] = $countArray;
					 * }
					 */
					/*
					 * Logger::getLogger("default")->debug($reportFields);
					 */
					$reportFields = $this->computeScores ( $countArray, $reportFields );
						
					Logger::getLogger ( "default" )->info ( $rpa );
				} else if (! empty ( $gf_form_id ) && $gf_form_id > 0) {
					Logger::getLogger ( "default" )->info ( "#### SINGLE source process" );
					$entries = $this->getGFEntries ( $gf_form_id, $maxentries, $custom_search_criteria );
					$reportFields = $this->buildReportFieldsForGF ( $gf_form_id, $type, isset($includeArray) ? $includeArray : null, isset($excludeArray) ? $excludeArray : null, $datasets_invert );
					Logger::getLogger ( "default" )->debug ( count ( $reportFields ) . ' graph(s) should be displayed' );
						
					if (empty ( $reportFields )) {
						$msg = "No data available for fields";
						Logger::getLogger ( "default" )->warn ( $msg );
						return $msg;
					} else {
						Logger::getLogger ( "default" )->trace ( $reportFields );
						$reportFields = $this->countDataFor ( $source, $entries, $reportFields, $data_conversion );
					}
				}
			} else if ($source == 'csv') {
				// FIXME : process URL instead of server files
				$msg = "Using CSV file : " . $url;
				Logger::getLogger ( "default" )->debug ( $msg );

				$csvArray = $this->csv_to_array ( $url );

				Logger::getLogger ( "default" )->debug ( count ( $csvArray ) . ' lines parsed' );
				/*
				 * if (empty($includeArray)) {
				 * $includeArray[] = 0;
				 * }
				 */

				// [csv2chartjs url="/var/www/wp-content/uploads/2017/03/Liste_des_titres_aides_en_2015.csv" xcol="1" ycol="2" header_start="1" header_size="2" type="bar" width="100%"]

				$colsNb = count ( $csvArray [0] );

				for($idx = 0; $idx < $colsNb; $idx ++) {
					if (! empty ( $includeArray )) {
						if (! in_array ( $idx, $includeArray )) {
							continue;
						}
					} else if (! empty ( $excludeArray )) {
						if (in_array ( $idx, $excludeArray )) {
							continue;
						}
					}
					$newGraph = $this->csvColumnAnalysis ( $csvArray, $header_start, $header_size, $xcol, $idx, $type, $compute, $maxentries );
					$newGraph ['graphType'] = $type;
						
					// [graphType] => pie
					$newGraph ['type'] = 'slider';
						
					$reportFields [] = $newGraph;
				}
				/*
				 * foreach ($includeArray as $colNb) {
				 * $reportFields[$colNb] = $this->csvColumnAnalysis($csvArray,$colNb,$type);
				 *
				 * }
				 */

				Logger::getLogger ( "default" )->debug ( $reportFields );
				// $reportFields = $this->countDataForCSV($reportFields);
			}
				
			// source data retrieved
				
			// setting colors
			$this->setColorPalette ( $color_set, $color_rand, $colors );
				
			$currentchartOptions = '';
			$currentchartData = '';
				
			$allCharts = '';
			$js_part = '';
			$currentchartData = '';
			// $allChartsNames = array();
			Logger::getLogger ( "default" )->debug ( "### Charts to display : " . count ( $reportFields ) );
			if (empty ( $reportFields )) {
				$msg = "No graph to display because no data";
				Logger::getLogger ( "default" )->debug ( $msg );
				return $msg;
			}
				
			foreach ( $reportFields as $chartId => $chartToPlot ) {
				// Logger::getLogger("default")->debug ( "+++ Creating chart for field " . $chartId );
				if (! is_int ( $chartId )) {
					Logger::getLogger("default")->error ( "not an id  " . $chartId );
					continue;
				}

				if (isset ( $chartToPlot ['no_answers'] ) && $chartToPlot ['no_answers'] == 1) {
					// no answers yet to field
					$currentChartMsg = '<h6>' . __ ( "No answers yet to field number" ) . ' ' . $chartId . ' : ' . $chartToPlot ['label'] . '</h6>';
					Logger::getLogger ( "default" )->warn ( $currentChartMsg );
					$allCharts .= $currentChartMsg;
					continue;
				}

				$chartType = $chartToPlot ['graphType'];
				// Logger::getLogger("default")->debug($chartToPlot);
				$string = preg_replace ( '/\s+/', '', $chartToPlot ['label'] );

				$currentChartId = $this->clean ( uniqid ( 'id_' ) . $chartId . '_' . $chartType );
				Logger::getLogger ( "default" )->debug ( "+++ Creating chart " . $currentChartId . ' with data size : ' . count ( $chartToPlot ) );
				Logger::getLogger ( "default" )->trace ( $chartToPlot );
				// $allChartsNames[] = $currentChartId;
				$chartOptionsId = 'Options_' . $currentChartId;
				$currentchartOptions = 'var ' . $chartOptionsId . ' = {';

				$tooltipContent = "tooltipLabel + ': '";
				if ($chartToPlot['multisets']) {
				    if ($type == 'pie'){
				        $tooltipContent = "tooltipLabel + ' / '+";
				    } else {
				        $tooltipContent = '';
				    }
				    $tooltipContent .= "datasetLabel + ': ' + tooltipData";
				    
				} else if ($data_conversion == "%") {
					$tooltipContent .= "+ tooltipData + '%'";
				} else {
					
					if ($tooltip_style == 'SUM') {
						// $tooltipContent = "tooltipLabel + ': ' + tooltipData";
						$tooltipContent .= "+ tooltipData";
					} else if ($tooltip_style == 'BOTH') {
						// $tooltipContent = "tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)'";
						$tooltipContent .= "+ tooltipData + ' (' + tooltipPercentage + '%)'";
					} else if ($tooltip_style == 'PERCENT') {
						// $tooltipContent = "tooltipLabel + ': ' + tooltipPercentage + '%'";
						$tooltipContent .= "+ tooltipPercentage + '%'";
					}
				}
				Logger::getLogger ( "default" )->debug ( "TOOLTIP: " . $tooltipContent );

				// $tooltipContent = $tooltipStyle
				$currentchartOptions .= "tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
                label: function(tooltipItem, data) {
                    var allData = data.datasets[tooltipItem.datasetIndex].data;
					var datasetLabel = data.datasets[tooltipItem.datasetIndex].label;
                    var tooltipLabel = data.labels[tooltipItem.index];
                    var tooltipData = allData[tooltipItem.index];
                    var total = 0;
                    for (var i in allData) {
                        total += allData[i];
                    }
                    var tooltipPercentage = Math.round((tooltipData / total) * 100);
					
                    return " . $tooltipContent . ";
				}
            }
        },";
				$multiRows = isset ( $chartToPlot ['gsurveyLikertEnableMultipleRows'] ) ? $chartToPlot ['gsurveyLikertEnableMultipleRows'] == 1 : false;
					
				if ($type == 'bar' || $type == 'horizontalBar') {
					$currentchartOptions .= "scales: {
    yAxes: [{
      id: 'y-axis-0',
      gridLines: {
        display: true,
        lineWidth: 1,
        color: 'rgba(0,0,0,0.30)'
      },
      ticks: {
        beginAtZero:true,
        mirror:false,
        suggestedMin: 0,

      },
      afterBuildTicks: function(chart) {

      }
    }],
    xAxes: [{
      id: 'x-axis-0',
      gridLines: {
        display: false
      },
      ticks: {
        beginAtZero: true,
		autoSkip: false
      }
    }]
},";
				}

				$chartTitleCleaned = str_replace ( '"', "", $chartToPlot ['label'] );
				$chartTitleCleaned = str_replace ( "'", "", $chartTitleCleaned );
				$chartTitleCleaned = $this->removeQuotes ( $chartTitleCleaned );

				Logger::getLogger ( "default" )->debug ( "Chart js options : " . $chart_js_options );
				if ($chart_js_options && count ( $chart_js_options ) > 0) {
					$currentchartOptions .= $chart_js_options;
				} else {
					$legendOption = "legend: {display: false },";
					$currentchartOptions .= $legendOption;
						
					$titleOption = "title:{display: true,text: '" . $chartTitleCleaned . "'}";
					$currentchartOptions .= $titleOption;
				}

				$currentchartOptions .= '}; ';
				Logger::getLogger ( "default" )->trace ( "Chart options : " . $currentchartOptions );
				$containerDimensions = 'width:' . $width . '; height:' . $height . '; ';
				$chartWrappingTag = 'div';
				$canvasStyle = '';
				$containerMargin = 'margin:' . $margin . ';';
				if (! empty ( $float ) && $float == true) {
					Logger::getLogger ( "default" )->debug ( "floating graphs... : " . $float );
					$additionalStyle = 'float:left;';
				} else {
					$additionalStyle = 'display:flex;';
				}

				if (! empty ( $center ) && $center == true) {
					Logger::getLogger ( "default" )->debug ( "center graphs... : " . $center );
					// $containerDimensions = 'height: 320px;width: 40%;';
					$containerMargin = 'margin: 0px auto;';
					// display: flex; max-width: 100%; /*! width: 100%; */ /*! height: auto; */ margin: 5px;
					$canvasStyle = 'display: block;margin: 0 auto;width: ' . $canvaswidth . '; height: ' . $canvasheight . ';';
					// display: block; width: 272px; height: 272px;
				}

				Logger::getLogger ( "default" )->debug ( $chartToPlot );

				Logger::getLogger ( "default" )->debug ( "switching type " . $type );
				switch ($type) {
					case 'pie' :
					case 'doughnut' :
					case 'bar' :
					case 'line' :
					case 'horizontalBar' :
						$currentchart = '<' . $chartWrappingTag . ' class="' . $align . ' ' . $class . ' gfchartsreports-wrap" style="' . $additionalStyle . 'max-width: 100%;' . $containerDimensions . $containerMargin . ' data-proportion="' . $relativewidth . '">';
						$currentchart .= '<canvas style="' . $canvasStyle . '" id="' . $currentChartId . '" height="' . $canvasheight . '" width="' . $canvaswidth . '" class="gfchartsreports_canvas" data-proportion="' . $relativewidth . '"></canvas></' . $chartWrappingTag . '>';
						$dataName = 'Data_' . $currentChartId;

						$currentchartData = 'var ' . $dataName . ' = {';
						$currentchartData .= 'labels : ["';
						$labelsDatasArray = array ();
						if (isset ( $chartToPlot ['labels'] ) && is_array ( $chartToPlot ['labels'] )) {
							// Logger::getLogger("default")->debug('Labels before clean : ' . implode("/", $chartToPlot['labels']));
							$labelsCleaned = array_map ( array (
									$this,
									'removeQuotesAndConvertHtml'
							), $chartToPlot ['labels'] );
							// Logger::getLogger("default")->debug ( 'Labels after clean : '.implode("/",$labelsCleaned ) );
							$all_numeric = true;
							foreach ( $labelsCleaned as $key ) {
								if (! (is_numeric ( $key ))) {
									$all_numeric = false;
									break;
								}
							}
							$idx = 0;
							foreach ( $labelsCleaned as $labelCleanedKey => $labelCleanedVal ) {
								$labelsDatasArray [$labelCleanedVal] = $chartToPlot ['data'] [$idx ++];
							}
							if ($all_numeric) {
								ksort ( $labelsDatasArray );
							}
								
							if (empty ( $labelsDatasArray )) {
								$currentchart = "No labels for graph with data " . $dataName;
								Logger::getLogger ( "default" )->error ( $currentchart );

								$allCharts .= $currentchart;
								continue;
							}
							// Logger::getLogger("default")->debug('Labels after clean : ' . implode("/", $labelsDatasArray));
							$currentchartData .= implode ( '","', array_keys ( $labelsDatasArray ) );
						}
						$currentchartData .= '"],';

						$currentchartDatasets = 'datasets : [';
						if (isset ( $multiRows ) && $multiRows) {
							$idx = 0;
							// count($chartToPlot ['labels'])
								
							foreach ( $chartToPlot ['datasets'] as $datasetName => $datasetDatas ) {
								if (isset ( $colors [$idx] )) {
									$currentColor = $colors [$idx];
									$idx ++;
								} else {
									$idx = 0;
									$currentColor = $colors [$idx];
								}
								$colorArray = array_fill ( 0, count ( $chartToPlot ['labels'] ), $currentColor );

								// Logger::getLogger("default")->debug ($datasetName);
								// Logger::getLogger("default")->debug ($datasetDatas);
								$currentDataset = '{';
								$currentDataset .= 'label: "' . $datasetName . '",';
								if (isset ( $datasetDatas ['data'] ) && is_array ( $datasetDatas ['data'] )) {
									$currentDataset .= 'data: [' . implode ( ",", array_values ( $datasetDatas ['data'] ) ) . '],';
								} else {
									Logger::getLogger ( "default" )->error ( "No data for " . $datasetName );
								}
								$currentDataset .= 'backgroundColor: ["' . implode ( '","', $colorArray ) . '"],';
								$currentDataset .= '},';

								$currentchartDatasets .= $currentDataset;
							}
						} else {
							$currentDataset = '{';
							$currentDataset .= '
                            		label: "' . $chartToPlot ['label'] . '",';
							if (isset ( $chartToPlot ['data'] ) && is_array ( $chartToPlot ['data'] )) {
								$currentDataset .= '
                                		data: [' . implode ( ",", array_values ( $labelsDatasArray ) ) . '],';
							} else {
								Logger::getLogger ( "default" )->debug ( "No labels for " . $datasetName );
							}
								
							if ($type === 'line') {
								$currentDataset .= 'backgroundColor: "' . $colors [0] . '",';
							} else {
								if (isset ( $colors )) {
									$colorArray = array ();
									$idxColor = 0;
									foreach ( $chartToPlot ['labels'] as $label ) {
										if (isset ( $colors [$idxColor] )) {
											// $colorArray[] = $colors[$idxColor];
											$idxColor ++;
										} else {
											$idxColor = 0;
										}
										$colorArray [] = $colors [$idxColor];
									}
								}

								$currentDataset .= 'backgroundColor: ["' . implode ( '","', $colorArray ) . '"],';
							}
							$currentDataset .= '}';
								
							$currentchartDatasets .= $currentDataset;
						}

						$currentchartDatasets .= ']';
						Logger::getLogger ( "default" )->trace ( $currentchartDatasets );
						$currentchartData .= $currentchartDatasets;
						if (empty ( $currentchartDatasets )) {
							$msg = "No datasets for graph with data " . $dataName;
							Logger::getLogger ( "default" )->debug ( $msg );
							return $msg;
						} else {
							Logger::getLogger ( "default" )->trace ( $currentchartDatasets );
						}
						$currentchartData .= '};';
						break;
							
					case 'table' :
						$allCharts .= apply_filters ( 'gfcr_display_table_from_chart_data_filter', $chartToPlot );
						continue;
					default :
						Logger::getLogger ( "default" )->warn ( "Unknown graph type : " . $type );
						continue;
				}


				if ($type != 'table' && empty ( $currentchartData )) {
					$msg = "No data for chart ".$chartTitleCleaned. ' / ' . $type;
					Logger::getLogger ( "default" )->debug ( $msg );
					return $msg;
				}
				/*
				 $json_options = json_encode ( $currentchartOptions, JSON_PRETTY_PRINT );
				 $json_datas = json_encode ( $currentchartData, JSON_PRETTY_PRINT );
				 */
				$optionsAndDatasVars = '
                		' . $currentchartOptions . '
                		' . $currentchartData;
				$js_part .= $optionsAndDatasVars;
				Logger::getLogger ( "default" )->trace ( $optionsAndDatasVars );
				$allCharts .= $currentchart;
				Logger::getLogger ( "default" )->trace ( $currentchart );
				$chartJson = '
                		window.gfchartsreports["' . $currentChartId . '"] = { options: ' . $chartOptionsId . ', data: ' . $dataName . ', type: \'' . $chartType . '\' };';
				Logger::getLogger ( "default" )->trace ( $currentchart );
				$js_part .= $chartJson;
			}
				
			// add script js
			$allCharts .= '<script type="text/javascript">' . 'window.gfchartsreports = window.gfchartsreports || {};' . $js_part . '</script>';
			Logger::getLogger ( "default" )->debug ( count ( $reportFields ) . ' charts created' );
			return $allCharts;
		}
		function replace_carriage_return($replace, $string) {
			return str_replace ( array (
					"\n\r",
					"\n",
					"\r"
			), $replace, $string );
		}
		function removeQuotesAndConvertHtml($str) {
			$res = preg_replace ( '/["]/', '', $str );
			$res = html_entity_decode ( $res );
			$res = $this->replace_carriage_return ( " ", $res );
				
			return $res;
		}
		function removeQuotes($string) {
			return preg_replace ( '/["]/', '', $string ); // Removes special chars.
		}
		function clean($string) {
			$string = str_replace ( ' ', '', $string ); // Replaces all spaces with hyphens.
				
			return preg_replace ( '/[^A-Za-z0-9\-]/', '', $string ); // Removes special chars.
		}
	}
}

new gfchartsreports();