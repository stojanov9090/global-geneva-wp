<?php


if (! class_exists('gfcr_data_conversion_plugin')) {
    class gfcr_data_conversion_plugin
    {
        function __construct()
        {
            //Logger::getLogger("default")->debug("construct gfcr_custom_search_criteria");
            Logger::getLogger("default")->debug ("Adding Module : ".__CLASS__);
            add_filter("gfcr_filter_fields_after_count", array(
                $this,
                "filter_fields_after_count"
            ),10 ,2);
        }
        
        function convertScores($scoresArray, $data_conversion) {
            $newScoreArray = array();
            Logger::getLogger("default")->debug($scoresArray);
            $dataSum = array_sum($scoresArray);
            Logger::getLogger("default")->debug("convertScores : ".$data_conversion. " sum: ".$dataSum);
            foreach ($scoresArray as $data => $dataCount) {
                //Logger::getLogger("default")->debug($dataCount . ' -> '.);
                $newValue = $data_conversion == "%" ? number_format($dataCount * 100 / $dataSum) : $dataCount;
                Logger::getLogger("default")->debug($dataCount . ' -> '.$newValue);
                $newScoreArray[$data] = $newValue;
                
            }
            
            Logger::getLogger("default")->debug($scoresArray);
            return $newScoreArray;
        }
        
        function filter_fields_after_count($reportFields, $data_conversion)
        {
            Logger::getLogger("default")->debug("filter_fields_after_count BEFORE");
            Logger::getLogger("default")->debug($reportFields);
            foreach ($reportFields as $key => $value) {
            	if (isset($reportFields[$key]['scores'])) {
            		$reportFields[$key]['scores'] = $this->convertScores($reportFields[$key]['scores'],$data_conversion);
            	} else {
            		Logger::getLogger("default")->warn('No scores item for field '.$key);
            	}
                
            }

            Logger::getLogger("default")->debug("filter_fields_after_count AFTER");
            Logger::getLogger("default")->debug($reportFields);
            return $reportFields;
        }
    }
}