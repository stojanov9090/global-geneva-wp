=== Gravity Forms Charts Reports ===
Contributors: munger41
Tags: gravity, forms, chart, chartsjs, graph, graphs, visualisation, survey, gravityforms, radio, field, checkbox, dropdown, text, likert, entry, stats, visualization, HTML5, canvas, pie chart, line chart, charts, chart js, plugin, widget, shortcode
Requires at least: 4.0
Tested up to: 4.7.2
Stable tag: 2.5.2

Create beautiful HTML5 charts from Gravity Forms submission data with a simple shortcode.

== Description ==

Create beautiful [Chartsjs](http://www.chartjs.org/ "Chartsjs") charts from [Gravity Forms](http://www.gravityforms.com/ "Gravity Forms") submission data with a simple shortcode.
Use all your past data to visualize advanced statistics in a few clics. [Please vote for next features you expect](http://gravityformschartsreports.termel.fr/vote-for-next-feature/ "Here")!

Tested up to Gravity Forms v2.1.3.6

[>> Demonstration site <<](http://gravityformschartsreports.termel.fr/ "Demonstration")

### Usage ###
the shortcode is 
`[gfchartsreports gf_form_id="form_id" include="fieldNb1,fieldNb2,fieldNb3,.." exclude="fieldNb4,fieldNb5,..." color_set="set" type="graphType" width="width(px|%)" float="true|false" chart_js_options="options"]`

where all parameters are optional:

* *gf_form_id* : expects the **form ID** in Gravity Forms (defaults to first form : id=1)
* *type* : is the type of graph, at the moment only **line,pie,doughnut,bar,horizontalBar** available (defaults to pie)
* *include* : expects the **field ids** to include (example : `include="1,3,4"`)
* *exclude* : expects the **field ids** to exlude (example : `exclude="2,4,6"`)
* *color_set* : expects the name of the color set : **blue,green,red,orange,purple** (defaults to a standard color set including different colors)
* *color_rand* : expects **true or false** and randomizes colors in color set if true (defaults to false)
* *width* : set width of graph(s) in pixels or percent (**width="100%"** or **width="250px"**), height will be adjusted automatically 
* *float* : (defaults to false) if true set `float:left` property to graph div container, and allow multiple column display
* *center* : (defaults to false) if true set center graphs inside container
* *maxentries* : by default, maximum 200 entries are retrieved from the gravity forms database, increase at your own risk (performances)
* *chart_js_options* : JSON like string like : **title: {display: true, text: 'My Funky Chart Title'}**. Use **'** to surround string parameters. [See here](http://www.chartjs.org/docs/#chart-configuration-creating-a-chart-with-options) for more informations
* *tooltip_style* : Control tooltip content, SUM displays total count of answers, PERCENT the % ratio among all answers, and BOTH displays both :)
* *data_conversion* : Set to "%" in order to convert in % all values. Previous parameter *tooltip_style* is disabled in this case. 
* *custom_search_criteria* : provide [custom search criteria](https://www.gravityhelp.com/documentation/article/api-functions/#get_entries) for gf submission query, for example **custom_search_criteria='{"status":"active","field_filters":{"0":{"key":"created_by","value":"current"}}}**

### Examples ###
Display all fields of form #1 as pie charts with custom [chartjs options](http://www.chartjs.org/docs/#chart-configuration-creating-a-chart-with-options) for titles:
`[gfchartsreports chart_js_options="title: {display: true, text: 'My Funky Chart Title', fontSize:28,fontFamily:'Arial',fontColor:'#00B88A',fontStyle:'bold',padding:20}"]`

Displays fields 4, 7 and 18 of gravity form #3 with bar charts. Default colors used.
`[gfchartsreports gf_form_id="3" include="4,7,18" type="bar"]`

Displays field 2 of gravity form #2 with default pie chart. Use red color set, with not randomization.
`[gfchartsreports gf_form_id="2" include="2" color_set="red"]`

Displays all but fields 4,7,18 of gravity form #8 with horizontalBar type charts. Use blue color set, randomized.
`[gfchartsreports gf_form_id="8" exclude="4,7,18" color_set="blue" color_rand="true" type="horizontalBar"]`

Test it live here : [>> Demonstration site <<](http://gravityformschartsreports.termel.fr/ "Demonstration")

== Installation ==

[>> Demonstration site <<](http://gravityformschartsreports.termel.fr/ "Demonstration")

### Easy ###
1. Search via plugins > add new.
2. Find the plugin listed and click activate.
3. Use the Shortcode


== Screenshots ==

[>> Demonstration site <<](http://gravityformschartsreports.termel.fr/ "Demonstration")

1. Chart report example 1
2. Chart report example 2
3. Chart report example 3

== Changelog ==

[>> Demonstration site <<](http://gravityformschartsreports.termel.fr/ "Demonstration")

* 2.5.4 - table module added

* 2.5.3.2 - csv reading improved

* 2.5.3.1 - cleaning and multi sources preparation

* 2.5.3 - data conversion module added

* 2.5.2 - bug fixed with carriage returns

* 2.5.1 - FATAL bug fixed with log4php

* 2.5 - log4php lib added, dynamic date criteria added

* 2.4.4 - disappearing labels fixed

* 2.4.3 - custom search criteria to be tested

* 2.4.2 - tooltip option added : tooltip_style="SUM|PERCENT|BOTH"

* 2.4.1 - towards csv graph

* 2.4 - unique id bug fixed

* 2.3.5 - new parameter : center="true|false" to center charts inside container

* 2.3.4 - new parameter : float="true|false" to organize charts as float elements in container

* 2.3.3 - remove legend by default, add it using chartjs option : `chart_js_options="legend: {display: false }"`

* 2.3.2 - now restores special characters in chart labels if any

* 2.3.0 - doughnut chart type supported

* 2.2.0 - Starting line type chart support

* 2.1.1 - Upgraded with Chartjs 2.5.0

* 2.1.0 - [Creating chart with options](http://www.chartjs.org/docs/#chart-configuration-title-configuration "chart with options") is now possible

* 2.0.1 - New images added as icon and banner

* 2.0.0 - Management of multi-rows survey fields of [Gravity Forms Survey Add-On](http://www.gravityforms.com/add-ons/survey/ "Gravity Forms Survey Add-On")

* 1.6.6 - Fix for survey fields in order to display the label instead of the text item

* 1.6.5 - [Gravity Forms Survey Add-On](http://www.gravityforms.com/add-ons/survey/ "Gravity Forms Survey Add-On") support added

* 1.6.3 - bugfix : labels for checkboxes now displayed

* 1.6.2 - auto-order if labels are numeric / remove non Active entries from fetch

* 1.6.1 - clean quotes in fields labels or titles

* 1.6 - manage fields with no answers yet

* 1.4 - bugfix : skip unknown fields

* 1.3 - new parameter maxentries

* 1.0 - First stable release.