<?php


if (! class_exists('gfcr_table_plugin')) {
    class gfcr_table_plugin
    {
        function __construct()
        {
            //Logger::getLogger("default")->debug("construct gfcr_custom_search_criteria");
            Logger::getLogger("default")->debug ("Adding Module : ".__CLASS__);
            add_filter("gfcr_display_table_from_chart_data_filter", array(
                $this,
                "display_table"
            ));
        }
        
        function convertScores($scoresArray, $data_conversion) {
        	/*
        	<h1>RWD List to Table</h1>
        	<table class="rwd-table">
        	<tr>
        	<th>Movie Title</th>
        	<th>Genre</th>
        	<th>Year</th>
        	<th>Gross</th>
        	</tr>
        	<tr>
        	<td data-th="Movie Title">Star Wars</td>
        	<td data-th="Genre">Adventure, Sci-fi</td>
        	<td data-th="Year">1977</td>
        	<td data-th="Gross">$460,935,665</td>
        	</tr>
        	<tr>
        	<td data-th="Movie Title">Howard The Duck</td>
        	<td data-th="Genre">"Comedy"</td>
        	<td data-th="Year">1986</td>
        	<td data-th="Gross">$16,295,774</td>
        	</tr>
        	<tr>
        	<td data-th="Movie Title">American Graffiti</td>
        	<td data-th="Genre">Comedy, Drama</td>
        	<td data-th="Year">1973</td>
        	<td data-th="Gross">$115,000,000</td>
        	</tr>
        	</table>
        	
        	<p>&larr; Drag window (in editor or full page view) to see the effect. &rarr;</p>
        	
        	*/
        }
        
        function display_table($chartToPlot)
        {
        	$resultTable = '<table class="rwd-table">';
            Logger::getLogger("default")->debug($chartToPlot);
            $resultTable .= '<tr>';
            $resultTable .= '<th>'.$chartToPlot['label'].'</th>';
            $resultTable .= '</tr>';             
                     
            foreach ($chartToPlot['labels'] as $label) {
            	$resultTable .= '<tr>';
            	$resultTable .= '<td data-th="'.$chartToPlot['label'].'">'.$label.'</td>';
            	$resultTable .= '</tr>';
            }
            $resultTable .= '</table>';            
            
            //Logger::getLogger("default")->debug("display_table AFTER");
            Logger::getLogger("default")->debug($resultTable);
            return $resultTable;
        }
    }
}